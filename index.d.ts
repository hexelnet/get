declare module '@hexelnet/get' {
  /**
   * get nested property
   *
   * @param data
   * @param {Object} data              Object to travel
   * @param {Array|string} path        Path Properties
   * @param {any} defaultValue         Optional default value when not found (Null by default)
   *
   * @returns {any}
   */
  function get(data: object, path: string, defaultValue: any = null): any

  export = get
}
