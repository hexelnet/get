
const isExisting = value =>
    typeof value !== 'undefined'
    && value !== null
    && value !== ''
    || value === 0


/**
 * if its not an array return the item in an array
 * @param {Object|Array|string} val
 * @returns {*}
 */
const arrayify = val => Array.isArray(val)
    ? val
    : typeof val === 'string'
        ? val.split('.')
        : [val]

/**
 * get nested property safely
 *
 * @example: get(props, ['user', 'posts', 0, 'comments'])
 * @example: get(props, 'user.posts.0.comments')
 *
 * @param {Object} o            Object to travel
 * @param {Array|string} p      path Properties
 * @param d                     Default value when not found
 */
const get = (o, p, d = null) => arrayify(p).reduce((xs, x) => (xs && isExisting(xs[x])) ? xs[x] : d, o)

export default get

