#get


get nested property in a safe way with the option to provide an optional value

##Examples

```javascript
const get = require('@hexelnet/get')
//Or
import get from '@hexelnet/get'

const bigDataObject ={
  user: {
    post: [
       { 
         title: 'This is a post',
         comments: ['Hello', 'Fluffykins'] 
       }
    ]
  }
}

get(bigDataObject, 'user.posts.0.comments') // ['Hello', 'Fluffykins']

/**
 * When an property doesn't exist
 */
get(bigDataObject, 'user.posts.2.comments') // null

/**
 * When an property doesn't exist and there's a default value
 */
get(bigDataObject, 'user.posts.2.comments', 'No comments found') // 'No comments found'


```

##Params: 

@param {Object}              Object to travel
@param {Array|string}        path Properties
@param {any}                 Optional default value when not found (Null by default)
